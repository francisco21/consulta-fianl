package com.worshop.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="consulta")
public class Consulta {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	public Integer getId(){
		return id;
	}
	public void setId(Integer id){
		this.id = id;
	}
	public Paciente getPaciente() {
		return paciente;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	/*public Date getFechaConsulta() {
		return fechaConsulta;
	}
	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}*/
	public List<DetalleConsulta> getDetalleconsulta() {
		return detalleconsulta;
	}
	public void setDetalleconsulta(List<DetalleConsulta> detalleconsulta) {
		this.detalleconsulta = detalleconsulta;
	}
	@ManyToOne
	@JoinColumn(name="id_paciente",nullable=false)
	private Paciente paciente;
	@ManyToOne
	@JoinColumn(name="id_medico",nullable=false)
	private Medico medico;
//	private Date fechaConsulta;
	@OneToMany(mappedBy="consulta", cascade= {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE},fetch=FetchType.LAZY,orphanRemoval=true)
	private List<DetalleConsulta> detalleconsulta;
	
	

}
