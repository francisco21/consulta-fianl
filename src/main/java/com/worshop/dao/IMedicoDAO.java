package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worshop.models.Medico;

@Repository
public interface IMedicoDAO extends JpaRepository<Medico, Integer>{

}
